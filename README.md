# Docker ERPNext

A dockerized version of [ERPNext](https://erpnext.com/).

## Getting started

Clone this repository or simply download the `docker-compose.yml` file and then run `docker compose up -d`.

Give it 5 - 10 minutes to boot up for the first time (it does all its configuration).

Open your browser and go to http://localhost:8099.

Log in as `Administrator` with password `admin`.

Then set up your account and company and start using ERPNExt!

## Backups

The create backups (and restore from them) ERPNext provides a backup functionality with [bench](https://frappeframework.com/docs/user/en/bench/reference/backup).

An alternative is to do a manual backup which is described as well.

The MySQL admin password is always `admin`.

### Backup with Bench

- Enter the running backend container and do backup:

```bash
docker exec -it docker-erpnext_backend_1 bash
bench backup
exit
```

- Copy database file from container to host:

```bash
docker cp docker-erpnext_backend_1:/home/frappe/frappe-bench/sites/frontend/private/backups/. backups/
```

### Restore with Bench

- Copy the backups from host to backend container:

```bash
docker cp backups/. docker-erpnext_backend_1:/home/frappe/frappe-bench/sites/frontend/private/backups/
```

- Enter backend container and restore the backup:

```bash
docker exec -it docker-erpnext_backend_1 bash
ls sites/frontend/private/backups
bench restore <name-of-backup>-frontend-database.sql.gz
```

### Backup Manually

- Enter the running database container and do backup:

```bash
docker exec -it docker-erpnext_db_1 bash
mysqldump -u root -p _5e5899d8398b5f7b > erpnext_database_backup.sql
exit
```

- Copy database file from database container to host:

```bash
docker cp docker-erpnext_db_1:/erpnext_database_backup.sql ./backups/
```

- Copy site files from backend container to host:

```bash
docker cp docker-erpnext_backend_1:/home/frappe/frappe-bench/sites ./backups/
```

### Restore Manually

- Copy database file from host to database container:

```bash
docker cp ./backups/erpnext_database_backup.sql docker-erpnext_db_1:/erpnext_database_backup.sql
```

- Enter database container and do restore:

```bash
docker exec -it docker-erpnext_db_1 bash
mysql -u root -p _5e5899d8398b5f7b < erpnext_database_backup.sql
exit
```

- Copy site files from host to backend container:

```bash
docker cp ./backups/sites docker-erpnext_backend_1:/home/frappe/frappe-bench/
```

## Direct Database Access

To be able to connect to the database with an external tool (such as [DBeaver](https://dbeaver.io/)) you need to uncomment the lines in the `db` section of `docker-compose.yml`. But only do this temporarly, as it exposes your database to the internet!
